import sys
import getopt
from analytics_worker import run

def command_help():
    print 'Usage: start_processor.py [options]\n'
    print 'Options:'
    print '\t -r, --action     Name of the action[run]'
    print '\t -h, --help       This help'
    sys.exit(1)

def execute(action):
    if action == 'run':
        run()
    else:
        print 'Error: Invalid or no action name. Check the help'
        command_help()

optlist, args = getopt.getopt(sys.argv[1:], 'r:h', ['action=', 'help'])

action = ''
for o, p in optlist:
    if o in ['-r', '--action']:
        action = p
    else:
        command_help()

execute(action)
