import redis
from settings import REDIS_HOST, REDIS_PORT, \
    REDIS_DB, REDIS_QUEUE_NAMESPACE, \
    REDIS_QUEUE_NAME

class RedisQueue(object):
    def __init__(self,):
        self.__db = redis.Redis(REDIS_HOST, REDIS_PORT, REDIS_DB)
        self.key = '%s:%s' %(REDIS_QUEUE_NAMESPACE, REDIS_QUEUE_NAME)
        
    def qsize(self,):
        return self.__db.llen(self.key)
    
    def empty(self,):
        return self.qsize() == 0
    
    def put(self, item):
        self.__db.rpush(self.key, item)

    def get(self, block=True, timeout=None):
        if block:
            item = self.__db.blpop(self.key, timeout=timeout)
        else:
            item = self.__db.lpop(self.key)
            
        if item:
            item = item[1]
        return item
        
    def get_nowait(self,):
        return self.get(False)

    def members_push(self, analytics_key_set, analytics_key):
        self.__db.rpush(analytics_key_set, analytics_key)

    def set_key(self, analytics_key):
        self.__db.set(analytics_key, 1)
        
    def incr_key(self, analytics_key):
        self.__db.incr(analytics_key)
        
    def get_key(self, analytics_key):
        return self.__db.get(analytics_key)

    def get_list_of_keys(self, analytics_key):
        return self.__db.keys('%s:%s' % (analytics_key, '*'))

    def get_lists(self, analytics_key_set):
        return self.__db.lrange(analytics_key_set, 0, -1)

    def set_description(self, analytics_key, description):
        self.__db.set(analytics_key, description)
        
    
