import datetime
import json
from flask import Flask, jsonify, make_response, request, \
    render_template
from queue import RedisQueue

app = Flask(__name__)

@app.route('/', methods=['GET'])
def index():
    return render_template('index.html', name='ali')

@app.route('/push', methods=['POST'])
def push():
    analytics_key = request.form.get('analytics_key')
    timestamp = request.form.get('timestamp')
    analytics_type = request.form.get('analytics_type')
    description = request.form.get('description')
    if not analytics_key:
        return make_response(jsonify({ 'analytics_key': 'Missing Required Parameter', 'success': False }), 400)
    if not timestamp:
        return make_response(jsonify({ 'timestamp': 'Missing Required Parameter', 'success': False }), 400)
    if not analytics_type:
        return make_response(jsonify({ 'analytics_type': 'Missing Required Parameter', 'success': False}), 400)
    if not description:
        return make_response(jsonify({ 'description': 'Missing Required Parameter', 'success': False}), 400)
    queue = RedisQueue()
    queue.put(json.dumps({'analytics_key': analytics_key, 'timestamp': timestamp, \
                           'analytics_type': analytics_type, 'description': description}))

    return make_response(jsonify({'success': True}), 200)

@app.route('/pop', methods=['GET'])
def pop():
    queue = RedisQueue()
    analytics_keys = queue.get_lists('analytics_keys')
    descriptions = {}
    for analytics_key in analytics_keys:
        description = queue.get_key('%s:%s' % ('description', analytics_key))
        descriptions[analytics_key] = description
        keys = queue.get_list_of_keys(analytics_key)
    if len(analytics_keys) > 0:
        first_key = analytics_keys[0]
    else:
        first_key = None

    return render_template('index.html', keys=list(set(analytics_keys)), first_key=first_key, \
                               descriptions=descriptions)        

@app.route('/fetch-tabular', methods=['GET'])
def fetch_tabular():
    analytics_key = request.args.get('analytics_key')
    queue = RedisQueue()
    keys = queue.get_list_of_keys(analytics_key)
    response = []
    for k in keys:
        day = k.split(':')[1]
        count = queue.get_key(k)
        temp = {}
        temp['title'] = count
        temp['start'] = day
        response.append(temp)

    return make_response(json.dumps(response), 200)

@app.route('/fetch-graphical', methods=['GET'])
def fetch_graphical():
    analytics_key = request.args.get('analytics_key')
    queue = RedisQueue()
    keys = queue.get_list_of_keys(analytics_key)
    response = {}
    for k in keys:
        day = k.split(':')[1]
        if k.split(':')[0] == analytics_key:
            count = queue.get_key(k)
            new_day = datetime.datetime.strptime(day, '%Y-%m-%d').strftime('%B %Y')
            if response.get(new_day):
                response[new_day] = response[new_day] + int(count)
            else:
                response[new_day] = int(count)
            
    return make_response(jsonify(response), 200)

if __name__ == '__main__':
    app.run(debug=True)
