DEBUG = False

# REDIS Configuration
REDIS_HOST = 'localhost'
REDIS_PORT = 6379
REDIS_DB = 1
TTL = 500
REDIS_QUEUE_NAMESPACE = 'analytics_queue'
REDIS_QUEUE_NAME = 'queue'

