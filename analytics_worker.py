import json
from queue import RedisQueue

queue = RedisQueue()

def run():
    while 1:
        queue_data = json.loads(queue.get())
        analytics_key = queue_data.get('analytics_key')
        timestamp = queue_data.get('timestamp')
        analytics_type = queue_data.get('analytics_type')
        description = queue_data.get('description')
        queue.members_push('analytics_keys', analytics_key)
        if queue.get_key('%s:%s' % (analytics_key, timestamp)):
            queue.incr_key('%s:%s' % (analytics_key, timestamp))
        else:
            queue.set_key('%s:%s' % (analytics_key, timestamp))
            
        if not queue.get_key('%s:%s' % ('description', analytics_key)):
            queue.set_description('%s:%s' % ('description', analytics_key), description)
            
            
    
        
        
