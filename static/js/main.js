function draw(canvasId, key)
{
    newdata = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    var labels = ["January 2014","February 2014","March 2014","April 2014","May 2014","June 2014","July 2014","August 2014","September 2014","October 2014","November 2014","December 2014"];
    console.log(key);
    $.ajax({
	type:'GET',
	url:"/fetch-graphical",
	data:{"analytics_key": key},
	success:function(data) {
	    $.each(labels, function( index, value ) {
		$.each(data, function( index0, value0 ) {
		    if (index0 == value) {
			console.log(value0);
			newdata[index] = value0;
		    }
		});
	    });
	    var lineChartData = {
		labels : labels,
		datasets : [
		    {
			fillColor : "rgba(84, 215, 136,0.4)",
			strokeColor : "rgba(84, 215, 136)",
			pointColor : "rgba(220,220,220,1)",
			pointStrokeColor : "#fff",
			data : newdata
		    }
		]
		
	    }
	    console.log(canvasId);
	    var myLine = new Chart(document.getElementById(canvasId).getContext("2d")).Line(lineChartData);
	},
	dataType:"json"
    });
}

function table(calendarId, key)
{
    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();
    
    $(calendarId).fullCalendar({
	editable: false,
	events: {
	    url: '/fetch-tabular',
	    cache: true,
	    type: 'GET',
	    data: {
		analytics_key: key
	    },
	    error: function() {
		alert('there was an error while fetching events!');
	    },
	    textColor: 'black'
	},
	eventBackgroundColor: 'rgb(169, 228, 169)'
    });
}

$(document).ready(function(){
    calendarId = '#calendar0';
    canvasId = 'canvas0';
    
    $('a[id^="list_"]').click(function() {
	nextId = this.id.split("_")[1];
	key = this.id.split("_")[2];
	calendarId = '#calendar'+nextId;
	canvasId = 'canvas'+nextId;
	$(calendarId).fullCalendar('destroy');
	table(calendarId, key);
	draw(canvasId, key);
	$("#first_key").text(key);
    });

    $('a[id^="tab-"]').click(function() {
	nextId = this.id.split("-")[1]
	calendarId = '#calendar'+nextId;
	canvasId = 'canvas'+nextId;
	key = $("#first_key").val();
	draw(canvasId, key);
	$(calendarId).fullCalendar('destroy');
	table(calendarId, key);
    });

    if( $(window).width()>$(window).height() && $(window).width()>768  ){
        var newHeight = $(window).height();
        $('.getHeight').css('height', newHeight);
    }
    key = $("#first_key").val();
    table(calendarId, key);
});

